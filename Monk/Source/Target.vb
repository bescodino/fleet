﻿Imports NLog

Public Class Target
    Inherits Targets.TargetWithLayout

    Private mMultiLineListBox As MultiLineListBox

    Sub New(pList As MultiLineListBox)
        mMultiLineListBox = pList
    End Sub
    
    Protected Overrides Sub Write(ByVal logEvent As LogEventInfo)

        If (mMultiLineListBox.IsHandleCreated) Then

            mMultiLineListBox.Invoke(Sub() mMultiLineListBox.AddItem(Layout.Render(logEvent)))

        End If

    End Sub


End Class
