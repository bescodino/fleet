﻿Imports System.IO
Imports System.Xml
Imports Monitoring
Imports System.Xml.Serialization

Public Class IOManager

    Private Shared mLock As New Object

    Public Shared ReadOnly Property UserPath As String
        Get
            Dim combinedPath As String = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Users")
            Directory.CreateDirectory(combinedPath)
            Return combinedPath
        End Get
    End Property


#Region "Settings"

    Public Shared Sub SaveSettings(pSettings As User, pFilePath As String)

        Dim serializer As New XmlSerializer(GetType(User))
        Dim writer As StreamWriter = Nothing

        Try
            writer = New StreamWriter(pFilePath)
            serializer.Serialize(writer, pSettings)
        Catch ex As Exception
            Logger.Instance.Error(String.Format("Cannot write to settings file: {0}", pFilePath))
        End Try

        If (writer IsNot Nothing) Then
            writer.Close()
        End If

    End Sub


    Public Shared Function LoadSettings(pFilePath As String) As User

        Dim serializer = New XmlSerializer(GetType(User))
        Dim loadedSettings As User = Nothing
        Dim reader As StreamReader

        Try

            SyncLock mLock

                reader = New StreamReader(pFilePath)
                loadedSettings = DirectCast(serializer.Deserialize(reader), User)
                reader.Close()

            End SyncLock

        Catch ex As InvalidOperationException

            Logger.Instance.Error(String.Format("Cannot load settings file: {0}", pFilePath))

            If (TypeOf ex.InnerException Is XmlException) Then
                Dim xmlEx As XmlException = DirectCast(ex.InnerException, XmlException)
                Logger.Instance.Error("Error parsing line " & xmlEx.LineNumber & ", at position " & xmlEx.LinePosition & ".")
            End If

        End Try

        Return loadedSettings

    End Function

#End Region

#Region "Jobs"


    Public Shared Sub SaveJob(pUser As User, Optional pFilePath As String = Nothing)

        Dim filePath As String
        Dim serializer As XmlSerializer
        Dim streamWriter As StreamWriter = Nothing


        If (String.IsNullOrEmpty(pFilePath)) Then
            filePath = Path.Combine(UserPath, pUser.mName & ".job")
        Else
            filePath = pFilePath
        End If

        serializer = New XmlSerializer(GetType(User))

        Try
            SyncLock mLock
                streamWriter = New StreamWriter(filePath)
                serializer.Serialize(streamWriter, pUser)
            End SyncLock
        Catch ex As Exception
            Logger.Instance.Error(String.Format("Cannot write to job file: {0}", filePath))
        End Try

        If (streamWriter IsNot Nothing) Then
            streamWriter.Close()
        End If

    End Sub


    Public Shared Function LoadJob(pFilePath As String) As User

        Dim serializer As XmlSerializer
        Dim reader As StreamReader
        Dim job As User = Nothing

        serializer = New XmlSerializer(GetType(User))

        Try

            SyncLock mLock
                reader = New StreamReader(pFilePath)
                job = serializer.Deserialize(reader)
                reader.Close()
            End SyncLock

        Catch ex As Exception
            If (reader IsNot Nothing) Then reader.Close()
            job = Nothing
            Logger.Instance.Warn(String.Format("Cannot load job file: {0}", pFilePath))
        End Try

        Return job

    End Function


#End Region


End Class
