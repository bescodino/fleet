﻿Imports System.Threading
Imports WebWhatsappBotCore

Public Class MainForm

    Private mServiceThreads As New List(Of Thread)
    Private mExterminatorThread As Thread
    Private mThreadsClosed As Boolean
    Private WithEvents mClosingTimer As New System.Windows.Forms.Timer

    Sub New()

        InitializeComponent()

        Controls.Add(_MultiLineListBox1)

    End Sub


    Public ReadOnly Property ListBox As MultiLineListBox
        Get
            Return _MultiLineListBox1
        End Get
    End Property


    Private Sub MainForm_Load(sender As Object, e As System.EventArgs) Handles Me.Load

        Logger.Instance.Info("Initializing...")


        mServiceThreads.Add(New Thread(New ThreadStart(AddressOf InputService.Run)) With {.Name = "InputService", .CurrentCulture = New System.Globalization.CultureInfo("en-US")})
        'mServiceThreads.Add(New Thread(New ThreadStart(AddressOf SendService.Run)) With {.Name = "SendService", .CurrentCulture = New System.Globalization.CultureInfo("en-US")})



        StartThreads()


        Logger.Instance.Info("Ready.")
        Logger.Instance.Debug("Debug ON")

    End Sub


    Private Sub MainForm_FormClosing(sender As Object, e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing

        If (Not mThreadsClosed) Then

            e.Cancel = True

            If (mExterminatorThread Is Nothing) Then

                mExterminatorThread = New Thread(New ThreadStart(AddressOf StopThreads)) With {.Name = "Exterminator"}
                mExterminatorThread.Start()

                mClosingTimer.Start()

            End If

        End If

    End Sub


    Private Sub mClosingTimer_Tick(sender As Object, e As EventArgs) Handles mClosingTimer.Tick

        If (Not mExterminatorThread.IsAlive) Then

            mClosingTimer.Stop()
            mClosingTimer = Nothing
            mThreadsClosed = True
            Close()

        End If

    End Sub


    Private Sub StartThreads()

        'Helper.StartDriver(Helper.mSender)

        For Each serviceThread In mServiceThreads
            serviceThread.Priority = ThreadPriority.Normal
            serviceThread.Start()
        Next

    End Sub


    Private Sub StopThreads()

        Logger.Instance.Info("Finalizing...")

        ThreadManager.CloseRequested = True

        For Each serviceThread In mServiceThreads
            serviceThread.Join()
        Next

    End Sub


End Class