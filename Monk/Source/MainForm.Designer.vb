﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class MainForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(MainForm))
        Me.MultiLineListBox1 = New Monk.MultiLineListBox()
        Me.SuspendLayout()
        '
        'MultiLineListBox1
        '
        Me.MultiLineListBox1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.MultiLineListBox1.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawVariable
        Me.MultiLineListBox1.FormattingEnabled = True
        Me.MultiLineListBox1.Location = New System.Drawing.Point(20, 20)
        Me.MultiLineListBox1.Name = "MultiLineListBox1"
        Me.MultiLineListBox1.ScrollAlwaysVisible = True
        Me.MultiLineListBox1.Size = New System.Drawing.Size(639, 333)
        Me.MultiLineListBox1.TabIndex = 0
        '
        'MainForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(679, 373)
        Me.Controls.Add(Me.MultiLineListBox1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "MainForm"
        Me.Padding = New System.Windows.Forms.Padding(20)
        Me.Text = "Monk"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents MultiLineListBox1 As Monk.MultiLineListBox
End Class
