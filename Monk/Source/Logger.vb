﻿Public Class Logger

    Private Shared mLogger As NLog.Logger
    Private Shared mLock As New Object
    Private Shared mDebugMode As Boolean
    Private Shared mDebugFolderPath As String = IO.Path.Combine(Application.StartupPath, "Output")

    Public Shared ReadOnly Property DebugMode() As Boolean
        Get
            Return mDebugMode
        End Get
    End Property

    Public Shared ReadOnly Property DebugFolderPath() As String
        Get
            IO.Directory.CreateDirectory(mDebugFolderPath)
            Return mDebugFolderPath
        End Get
    End Property


    Public Shared ReadOnly Property Instance As NLog.Logger
        Get
            SyncLock mLock
                Return mLogger
            End SyncLock
        End Get
    End Property


    Public Shared Sub Setup(pMainForm As MainForm)

        Dim loggingConfig As New NLog.Config.LoggingConfiguration
        Dim fileTarget As New NLog.Targets.FileTarget
        Dim controlTarget As New Target(pMainForm.ListBox)
        Dim fileRule As NLog.Config.LoggingRule
        Dim controlRule As NLog.Config.LoggingRule
        Dim logLevel As NLog.LogLevel

        mDebugMode = (Configuration.ConfigurationManager.AppSettings("Debug").ToLower = "true")

        If (mDebugMode) Then

            logLevel = NLog.LogLevel.Debug

        Else

            logLevel = NLog.LogLevel.Info

        End If

        controlTarget.Layout = "${date:format=yyyy-MM-dd HH\:mm\:ss} [${level}] ${message}${newline}"

        fileTarget.FileName = IO.Path.Combine(Application.StartupPath, "Monk.log")
        fileTarget.Layout = "${date:format=yyyy-MM-dd HH\:mm\:ss} [${level}] ${message}"

        fileRule = New NLog.Config.LoggingRule("*", logLevel, fileTarget)
        controlRule = New NLog.Config.LoggingRule("*", logLevel, controlTarget)


        loggingConfig.LoggingRules.Add(fileRule)
        loggingConfig.LoggingRules.Add(controlRule)

        NLog.LogManager.Configuration = loggingConfig

        mLogger = NLog.LogManager.GetLogger(Process.GetCurrentProcess().ProcessName)

    End Sub

End Class
